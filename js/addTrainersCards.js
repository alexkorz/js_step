import DATA from "./trainersData.js";
import {
  sortButtons,
  sorting,
  trainerCardsContainer,
  trainerCardTemplate,
  filtersSubmit,
  filtersInput,
  filtersLabels
} from "./elements.js";
import {openModal} from "./openModalWindow.js";
import {closeModal} from "./closeModalWindow.js";
import {sortCards} from "./cardsSort.js";
import {filterCards} from "./cardsFilter.js";

let filteredCardsForRender = null;

export function addTrainersCards(cards) {
  document.querySelectorAll(".trainer").forEach(el => el.remove())
  cards.forEach(trainer => {
    const trainerCard = trainerCardTemplate.content.cloneNode(true);
    trainerCard.querySelector(".trainer__name").textContent = trainer["first name"] + " " + trainer["last name"];
    trainerCard.querySelector(".trainer__img").setAttribute("src", trainer.photo);
    trainerCard.querySelector(".trainer").setAttribute("data-lastname", trainer["last name"]);
    trainerCard.querySelector(".trainer").setAttribute("data-experience", parseInt(trainer.experience));
    trainerCard.querySelector(".trainer__show-more").addEventListener("click", function (event) {
      openModal(event);
      document.querySelector(".modal__close").addEventListener("click", function () {
        closeModal();
      })
    })
    trainerCardsContainer.append(trainerCard);
  })
}

export function sortedCardsRender() {
  sorting.addEventListener("click", function (event) {
    sortButtons.forEach(function (element) {
      element.classList.remove("sorting__btn--active");
    })

    /* getting button's text to choose the way of sorting */
    const sortingOption = event.target.textContent.trim();
    /* initializing the array of objects with data for cards' sorting */
    let dataForSort = [...DATA];
    if(filteredCardsForRender === null) {
      dataForSort = [...DATA];
    } else {
      dataForSort = [...filteredCardsForRender];
    }

    if (sortingOption === "ЗА ПРІЗВИЩЕМ") {
      event.target.classList.add("sorting__btn--active");
      sortCards(dataForSort, sortingOption);
      addTrainersCards(dataForSort)
    } else if (sortingOption === "ЗА ДОСВІДОМ") {
      event.target.classList.add("sorting__btn--active");
      sortCards(dataForSort, sortingOption);
      addTrainersCards(dataForSort)
    } else if (sortingOption === "ЗА замовчуванням") {
      event.target.classList.add("sorting__btn--active");
      addTrainersCards(dataForSort)
    }
  })
}

export function getFilters () {
  filtersSubmit.addEventListener("click", function (e) {
    e.preventDefault();
    /* creating array with two keys to save filters' settings for further actions */
    let filteredOptions = [];
    for(let i = 0; i < filtersInput.length && i < filtersLabels.length; i++) {
      if(filtersInput[i].checked) {
        /* filterName - name of the filter on the page from <label> tag,
           filterType - name of the filter from <input> tag */
        filteredOptions.push({filterName: filtersLabels[i].textContent.toLowerCase().trim(), filterType: filtersInput[i].name});
      }
    }
    addTrainersCards(filterCards(DATA, filteredOptions));
    /* saving filtered cards in the separate array to make them available for sorting */
    filteredCardsForRender = filterCards(DATA, filteredOptions);
    console.log(filteredCardsForRender)
  })
}
getFilters();


export function closeModal() {
  document.querySelector(".modal").remove();
  document.querySelector('body').classList.remove('disable-scrolling');
}

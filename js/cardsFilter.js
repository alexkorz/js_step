export function filterCards (cards, filterOptions) {
  let filteredCards = [];
  for(let i = 0; i < filterOptions.length; i++) {
    if(filterOptions[i].filterType === "direction"){
      if(filterOptions[i].filterName === "всі"){
        filteredCards = cards;
      } else {
        filteredCards = cards.filter(card => card.specialization.toLowerCase().trim() === filterOptions[i].filterName);
      }
    } else if(filterOptions[i].filterType === "category") {
      if(filterOptions[i].filterName === "всі"){
        filteredCards = filteredCards;
      } else {
        filteredCards = filteredCards.filter(card => card.category.toLowerCase().trim() === filterOptions[i].filterName);
      }
    }
  }
  return filteredCards;
}

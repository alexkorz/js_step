

export function sortCards(cards, sortingOption) {
  if (sortingOption === "ЗА ПРІЗВИЩЕМ") {
    cards.sort((a, b) => {
        return a["last name"].localeCompare(b["last name"]);
    })
  } else if (sortingOption === "ЗА ДОСВІДОМ") {
    cards.sort( (a, b) => {
      a = parseInt(a.experience);
      b = parseInt(b.experience);
      if (a < b) {
        return 1;
      }
      if (a > b) {
        return -1;
      }
      return 0;
    })
  } else if (sortingOption === "ЗА замовчуванням") {

  }
}


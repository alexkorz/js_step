import {modalTemplate, pageWrapper} from "./elements.js";
import DATA from "./trainersData.js"

export function openModal (element) {
  document.querySelector('body').classList.add('disable-scrolling');
  const modalWindow = modalTemplate.content.cloneNode(true);
  const relatedTrainerElement = element.target.closest(".trainer");
  const relatedTrainerData = DATA.find(trainer =>
    relatedTrainerElement.querySelector(".trainer__name").textContent === (trainer["first name"] + " " + trainer["last name"]))
  modalWindow.querySelector(".modal__img").setAttribute("src", relatedTrainerData.photo);
  modalWindow.querySelector(".modal__name").textContent = `${relatedTrainerData["first name"]} ${relatedTrainerData["last name"]}`;
  modalWindow.querySelector(".modal__point--category").textContent = `Категорія: ${relatedTrainerData.category}`;
  modalWindow.querySelector(".modal__point--experience").textContent = `Досвід: ${relatedTrainerData.experience}`;
  modalWindow.querySelector(".modal__point--specialization").textContent = `Спеціалізація: ${relatedTrainerData.specialization}`;
  modalWindow.querySelector(".modal__text").textContent = `Опис: ${relatedTrainerData.description}`;

  pageWrapper.appendChild(modalWindow);
}
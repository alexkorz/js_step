"use strict";

import DATA from "./trainersData.js";
import {sortingSection, sidebarSection} from "./elements.js";
import {addTrainersCards, sortedCardsRender} from "./addTrainersCards.js";



document.addEventListener("DOMContentLoaded", function () {
	sortingSection.removeAttribute('hidden');
	sidebarSection.removeAttribute('hidden');
	addTrainersCards(DATA);
	sortedCardsRender();
})
// document.querySelectorAll(".filters__label").forEach(el => console.log(el.textContent.toLowerCase().trim()));


